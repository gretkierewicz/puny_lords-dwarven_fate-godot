extends Node
class_name PersonalityInitializationSystem

@export var _min_interaction_waiting_threshold: float
@export var _max_interaction_waiting_threshold: float

var _rng = RandomNumberGenerator.new()

func _on_entities_child_entered_tree(node: Node) -> void:
    if not node.is_in_group(PersonalityComponent.type):
        return

    var personality_component = node.get_component(PersonalityComponent) as PersonalityComponent
    personality_component.extraversion = self._rng.randf_range(0, 1)

    if not node.is_in_group(InteractionParticipantComponent.type):
        return

    var participant_component = node.get_component(InteractionParticipantComponent) as InteractionParticipantComponent
    if participant_component.waiting_threshold != -1:
        return

    var initiative_range = self._max_interaction_waiting_threshold - self._min_interaction_waiting_threshold
    # extraversion is inversely propotional to interaction wait
    var factors_sum = 1 - personality_component.extraversion
    participant_component.waiting_threshold = self._min_interaction_waiting_threshold + initiative_range * factors_sum

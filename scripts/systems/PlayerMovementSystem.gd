extends Node
class_name PlayerMovementSystem

@export var _speed_factor: float
@export var _player: CharacterBody2D

func _physics_process(_delta: float) -> void:
    var direction = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")

    if direction:
        self._player.velocity = direction * self._speed_factor
    else:
        self._player.velocity.x = move_toward(self._player.velocity.x, 0, self._speed_factor)
        self._player.velocity.y = move_toward(self._player.velocity.y, 0, self._speed_factor)

    self._player.move_and_slide()

extends Node
class_name TablePassiveScoreSystem

signal score_updated(value: float)
signal score_gained(value: float, entity: Entity)

@export var _score_update_interval: float = 2
@export var _pasive_score_factor: float = 1

# builtin methods

func _ready() -> void:
    var timer = Timer.new()
    timer.autostart = true
    timer.wait_time = self._score_update_interval
    timer.timeout.connect(self._gain_passive_score_per_table)
    self.add_child(timer)

    self.score_updated.emit(PersistentDataManager.score)

# private methods

func _gain_passive_score_per_table() -> void:
    var table_data = {}
    for table in self.get_tree().get_nodes_in_group(EntityTag.table_entity):
        table_data[table.name] = {"score": 0, "node": table}

    for customer in self.get_tree().get_nodes_in_group(EntityTag.sits_at_table):
        var table_reservation_component = customer.get_component(TableReservationComponent) as TableReservationComponent
        table_data[table_reservation_component.table_id]["score"] += 1

    for table_id in table_data:
        var score_gain = self._pasive_score_factor * table_data[table_id]["score"]
        self.score_gained.emit(score_gain, table_data[table_id]["node"])
        PersistentDataManager.score += score_gain
        self.score_updated.emit(PersistentDataManager.score)

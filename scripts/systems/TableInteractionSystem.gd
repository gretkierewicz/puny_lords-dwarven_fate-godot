extends Node
class_name TableInteractionSystem

@export var _entities: Node
@export var _interaction_time: float = 4

var _interaction_scene = preload("res://scenes/entity/Interaction.tscn")

# buildin methods

func _physics_process(delta: float) -> void:
    for entity in self.get_tree().get_nodes_in_group(EntityTag.sits_at_table):
        self._attempt_interaction_initiation(delta, entity)

# private methods        

func _attempt_interaction_initiation(delta: float, initiator: Entity) -> void:
    var participant_component = initiator.get_component(InteractionParticipantComponent) as InteractionParticipantComponent
    if participant_component.current_interaction:
        participant_component.waiting_time = 0
        return

    participant_component.waiting_time += delta
    if participant_component.waiting_time < participant_component.waiting_threshold:
        return

    var participants = self._find_participants(initiator)
    if participants.size() < 2:
        # requires someone except initiator
        return

    var interaction = self._interaction_scene.instantiate()
    self._inject_interaction(interaction, participants)
    self._update_data_component(interaction, initiator, participants)
    self._entities.add_child(interaction)

func _find_participants(initiator: Entity) -> Array:
    var participants = []
    var reservation_component = initiator.get_component(TableReservationComponent) as TableReservationComponent
    var initiator_table_id = reservation_component.table_id

    for entity in self.get_tree().get_nodes_in_group(TableReservationComponent.type):
        var entity_reservation_component = entity.get_component(TableReservationComponent) as TableReservationComponent
        if entity_reservation_component.table_id != initiator_table_id:
            # constraint to local-table interactions only
            continue

        var entity_participant_component = entity.get_component(InteractionParticipantComponent) as InteractionParticipantComponent
        if entity_participant_component.current_interaction:
            # prevents dropping current active interaction
            continue

        participants.append(entity)

    return participants

func _inject_interaction(interaction: Entity, participants: Array) -> void:
    for entity in participants:
        var participant_component = entity.get_component(InteractionParticipantComponent) as InteractionParticipantComponent
        participant_component.current_interaction = interaction

func _update_data_component(interaction: Entity, initiator: Entity, participants: Array) -> void:
        var data_component = interaction.get_component(InteractionDataComponent) as InteractionDataComponent
        data_component.initiator = initiator
        data_component.participants = participants
        data_component.time_remaining = self._interaction_time

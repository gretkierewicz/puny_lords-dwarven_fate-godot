extends Node
class_name FollowPlayerSystem

@export var _player: CharacterBody2D

func _physics_process(_delta: float) -> void:
    for entity in self.get_tree().get_nodes_in_group(EntityTag.follows_player):
        var customer = entity as CharacterBody2D
        var vector_toward_player: Vector2 = self._player.position - customer.position

        if vector_toward_player.length() < 20:
            customer.velocity = Vector2()
            return

        var direction: Vector2 = vector_toward_player.normalized()
        customer.velocity = direction * 200
        customer.move_and_slide()

extends Node
class_name TooltipDisplaySystem

@export var _tooltip: Tooltip
@export var _player: Node2D
@export var _pointer: Node2D
@export var _pointer_distance: float
@export var _extraversion_descriptions: Array[String]
@export var _happiness_descriptions: Array[String]

# buildin methods

func _process(_delta: float) -> void:
    var customers = self.get_tree().get_nodes_in_group(EntityTag.customer_entity)
    if not customers:
        return

    var closest_customer = self._get_closest_entity(customers)
    self._populate_tooltip(closest_customer)
    self._point_node(closest_customer)

# private methods

func _get_closest_entity(entities: Array) -> Entity:
    if entities.size() == 1:
        return entities[0]

    var smallest_distance: float = INF
    var closest_entity

    for entity in entities:
        var entity_distance = entity.position.distance_to(self._player.position)
        if entity_distance >= smallest_distance:
            continue
        
        smallest_distance = entity_distance
        closest_entity = entity

    return closest_entity

func _populate_tooltip(customer: Entity) -> void:
    self._tooltip.clear()
    self._tooltip.set_header("Customer")
    self._tooltip.add_heading("Personality")
    self._tooltip.add_property(self._get_extraversion_description(customer))
    self._tooltip.add_heading("Emotional State")
    self._tooltip.add_property(self._get_happiness_description(customer))
    self._tooltip.visible = true

func _get_extraversion_description(customer: Entity) -> String:
    var personality_component = customer.get_component(PersonalityComponent) as PersonalityComponent
    return self._pick_description(
        personality_component.extraversion, self._extraversion_descriptions
    )

func _get_happiness_description(customer: Entity) -> String:
    var emotional_state_component = customer.get_component(EmotionalStateComponent) as EmotionalStateComponent
    return self._pick_description(
        emotional_state_component.happiness, self._happiness_descriptions
    )

func _pick_description(value: float, descriptions: Array) -> String:
    var options_count = descriptions.size()
    var step = 1.0 / options_count

    for i in range(options_count):
        if value < i * step:
            continue
        if value > (i + 1) * step:
            continue

        return descriptions[i]

    return ""

func _point_node(node: Node2D) -> void:
    var direction = self._player.position.direction_to(node.position)
    self._pointer.position = node.position - direction * self._pointer_distance
    self._pointer.rotation = direction.angle()
    self._pointer.visible = true

extends Node
class_name TableAssignmentSystem

@export var _min_assignment_distance: float

func _process(_delta: float) -> void:
    for guest in self.get_tree().get_nodes_in_group(EntityTag.follows_player):
        var nearby_table_id = self._get_nearby_table_id(guest)
        if not nearby_table_id:
            continue

        var seats_taken = self._get_table_seats_taken(nearby_table_id)
        var seat_placement = self._get_free_seat_placement(seats_taken)
        var guest_table_reservation = guest.get_component(TableReservationComponent) as TableReservationComponent
        guest_table_reservation.seat_placement = seat_placement
        if not guest_table_reservation.has_seat_assigned:
            # failed to assign new seat
            continue
        
        guest_table_reservation.table_id = nearby_table_id
        EntityTag.set_tag(guest, EntityTag.takes_seat)

# private methods

func _get_nearby_table_id(guest: Entity) -> String:
    for table in self.get_tree().get_nodes_in_group(EntityTag.table_entity):
        if PositionUtils.is_in_range(guest, table, self._min_assignment_distance):
            return table.name

    return ""

func _get_table_seats_taken(table_id: String) -> Array[Vector2]:
    var seats_taken: Array[Vector2] = []

    for entity in self.get_tree().get_nodes_in_group(TableReservationComponent.type):
        var table_reservation = entity.get_component(TableReservationComponent) as TableReservationComponent
        if not table_reservation.has_seat_assigned:
            continue

        if table_reservation.table_id != table_id:
            continue
        
        seats_taken.append(table_reservation.seat_placement)

    return seats_taken

func _get_free_seat_placement(seats_taken: Array[Vector2]) -> Vector2:
    for seat_placement in self._get_possible_seat_placements():
        if seat_placement in seats_taken:
            continue
        
        return seat_placement
    
    return Vector2()

func _get_possible_seat_placements() -> Array[Vector2]:
    return [Vector2.UP, Vector2.DOWN, Vector2.LEFT, Vector2.RIGHT]

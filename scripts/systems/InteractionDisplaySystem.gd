extends Node
class_name InteractionDisplaySystem

@export var _effects: Node
@export var _popup_start_offset = Vector2(0, 0)
@export var _popup_end_offset = Vector2(0, -20)
@export var _popup_life_time: float = 1

var _initiator_popup_scene = preload("res://scenes/gui/interaction_initiator_popup.tscn")
var _participant_popup_scene = preload("res://scenes/gui/interaction_participant_popup.tscn")

# signal handling

func _on_entities_child_entered_tree(node: Node) -> void:
    if not node.is_in_group(EntityTag.interaction_entity):
        return

    var data_component = node.get_component(InteractionDataComponent) as InteractionDataComponent
    self._create_initiator_popup(data_component.initiator)
    self._create_participant_popups(data_component)

# private methods

func _create_initiator_popup(initiator: Node2D) -> void:
    var popup = self._initiator_popup_scene.instantiate()
    self._effects.add_child(popup)
    self._animate_popup(popup, initiator.position)

func _create_participant_popups(data_component: InteractionDataComponent) -> void:
    for participant in data_component.participants:
        if participant == data_component.initiator:
            continue

        var popup = self._participant_popup_scene.instantiate()
        self._effects.add_child(popup)
        self._animate_popup(popup, participant.position)

func _animate_popup(popup: Node2D, start_position: Vector2) -> void:
    popup.position = start_position + self._popup_start_offset

    var tween = popup.create_tween()
    tween.tween_property(
        popup,
        "position",
        start_position + self._popup_end_offset,
        self._popup_life_time,
    )
    tween.tween_callback(popup.queue_free)

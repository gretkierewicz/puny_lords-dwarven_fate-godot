extends Node
class_name SpawnCustomerSystem

@export var _tavern_entry_position: Vector2
@export var _guest_spawn_delay: float
@export var _entities: Node
@export var _spawn_customer_limit: int

var _spawn_customer_timer: Timer
var _customer_scene = preload("res://scenes/entity/customer.tscn")
var _customer_counter: int = 0

func _ready() -> void:
    self._spawn_customer_timer = Timer.new()
    self._spawn_customer_timer.one_shot = true
    self._spawn_customer_timer.timeout.connect(self._on_spawn_customer_timer_timeout)
    self.add_child(self._spawn_customer_timer)

func _process(_delta: float) -> void:
    if self._is_anyone_waiting_on_entry():
        return

    if not self._spawn_customer_timer.is_stopped():
        return

    self._spawn_customer_timer.start(self._guest_spawn_delay)

# private methods

func _on_spawn_customer_timer_timeout() -> void:
    if self._customer_counter >= self._spawn_customer_limit:
        return

    var guest = self._create_guest()
    self._set_animation(guest)
    self._customer_counter += 1
    EntityTag.set_tag(guest, EntityTag.waits_on_entry)

func _is_anyone_waiting_on_entry() -> bool:
    return not self.get_tree().get_nodes_in_group(EntityTag.waits_on_entry).is_empty()

func _create_guest() -> CharacterBody2D:
    var guest = self._customer_scene.instantiate() as CharacterBody2D

    self._entities.add_child(guest)
    guest.position = _tavern_entry_position

    return guest

func _set_animation(guest: Node) -> void:
    var animated_sprite: AnimatedSprite2D = guest.find_child("AnimatedSprite2D")
    # there are 11 different sprites in animation, cycle through them
    animated_sprite.frame = self._customer_counter % 11
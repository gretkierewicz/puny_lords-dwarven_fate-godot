extends Node
class_name ScoreDisplaySystem

@export var _effects: Node
@export var _position_offset_factor = 18
@export var _popup_life_time: float = 3

var _score_scene = preload("res://scenes/gui/score_popup.tscn")

# signal handling

func _on_score_gained(value: float, entity: Entity) -> void:
    if not value:
        return

    self._create_score_popup(value, entity)

# private methods

func _create_score_popup(score_value: float, entity: Entity) -> void:
    var offset_vector = Vector2()
    if entity.is_in_group(TableReservationComponent.type):
        var table_reservation_component = entity.get_component(TableReservationComponent) as TableReservationComponent
        offset_vector = table_reservation_component.seat_placement

    var popup = self._score_scene.instantiate()
    popup.set_value(score_value)
    var popup_offset = offset_vector * self._position_offset_factor
    popup.position = entity.position + popup_offset

    self._effects.add_child(popup)
    self._animate_popup(popup)

func _animate_popup(popup: Control) -> void:
    var tween = popup.create_tween()
    tween.tween_property(popup, "modulate:a", 0, self._popup_life_time)
    tween.tween_callback(popup.queue_free)

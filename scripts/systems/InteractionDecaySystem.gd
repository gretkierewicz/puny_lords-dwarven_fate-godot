extends Node
class_name InteractionDecaySystem

# buildin methods

func _physics_process(delta: float) -> void:
    for interaction in self.get_tree().get_nodes_in_group(EntityTag.interaction_entity):
        var data_component = interaction.get_component(InteractionDataComponent) as InteractionDataComponent
        data_component.time_remaining -= delta

        if data_component.time_remaining > 0:
            continue

        self._close_interaction(interaction)

# private methods

func _close_interaction(interaction: Entity):
    var data_component = interaction.get_component(InteractionDataComponent) as InteractionDataComponent

    for participant in data_component.participants:
        var participant_component = participant.get_component(InteractionParticipantComponent) as InteractionParticipantComponent
        participant_component.current_interaction = null

    interaction.queue_free()

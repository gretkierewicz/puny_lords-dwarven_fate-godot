extends Node
class_name GuestTakesSeatSystem

@export var _seat_distance: float

func _process(_delta: float) -> void:
    for guest in self.get_tree().get_nodes_in_group(EntityTag.takes_seat):
        var guest_table_reservation = guest.get_component(TableReservationComponent) as TableReservationComponent
        if not guest_table_reservation.has_seat_assigned:
            continue

        self._align_with_seat(guest, guest_table_reservation)
        EntityTag.set_tag(guest, EntityTag.sits_at_table)

# private methods

func _align_with_seat(node: Node2D, table_reservation: TableReservationComponent) -> void:
    var table_node: Node2D = self.get_tree().root.find_child(table_reservation.table_id, true, false)
    var seat_relative_position = table_reservation.seat_placement * self._seat_distance
    node.position = table_node.position + seat_relative_position

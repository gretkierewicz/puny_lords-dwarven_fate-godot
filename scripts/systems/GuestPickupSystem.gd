extends Node
class_name GuestPickupSystem

@export var _min_pickup_distance: float
@export var _player: CharacterBody2D

func _process(_delta: float) -> void:
    if self.get_tree().get_nodes_in_group(EntityTag.follows_player):
        # limit setting following to one guest only
        return

    for guest in self.get_tree().get_nodes_in_group(EntityTag.waits_on_entry):
        if PositionUtils.is_in_range(self._player, guest, self._min_pickup_distance):
            EntityTag.set_tag(guest, EntityTag.follows_player)

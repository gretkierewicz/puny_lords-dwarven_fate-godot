extends Node
class_name EmotionalResponseSystem

@export var _happiness_factor: float
@export var _absolute_acceptable_difference: float

func _on_entities_child_entered_tree(node: Node) -> void:
    if not node.is_in_group(EntityTag.interaction_entity):
        return
    
    node = node as Entity
    var interaction_data = node.get_component(InteractionDataComponent) as InteractionDataComponent
    var emotional_state = interaction_data.initiator.get_component(EmotionalStateComponent) as EmotionalStateComponent

    emotional_state.happiness += self._happiness_factor

    for participant in interaction_data.participants:
        emotional_state = participant.get_component(EmotionalStateComponent) as EmotionalStateComponent
        var interaction_participant_component = participant.get_component(InteractionParticipantComponent) as InteractionParticipantComponent
        var acceptable_waiting_time = interaction_participant_component.waiting_time + self._absolute_acceptable_difference

        var multiply_factor = -1
        if acceptable_waiting_time > interaction_participant_component.waiting_threshold:
            multiply_factor = +1

        emotional_state.happiness = emotional_state.happiness + multiply_factor * self._happiness_factor

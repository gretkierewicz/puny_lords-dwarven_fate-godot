extends Node
class_name InteractionScoreSystem

signal score_updated(value: float)
signal score_gained(value: float, entity: Entity)

@export var _initiator_score_factor: float = 2
@export var _participant_score_factor: float = 1

# signal handling

func _on_entities_child_entered_tree(node: Node) -> void:
    if not node.is_in_group(EntityTag.interaction_entity):
        return
    
    self._gain_score_from_interaction(node)

# private methods

func _gain_score_from_interaction(interaction: Entity) -> void:
    var interaction_data_component = interaction.get_component(InteractionDataComponent) as InteractionDataComponent

    PersistentDataManager.score += self._initiator_score_factor
    self.score_gained.emit(self._initiator_score_factor, interaction_data_component.initiator)
    self.score_updated.emit(PersistentDataManager.score)

    for participant in interaction_data_component.participants:
        if participant == interaction_data_component.initiator:
            continue
        
        PersistentDataManager.score += self._participant_score_factor
        self.score_gained.emit(self._participant_score_factor, participant)
        self.score_updated.emit(PersistentDataManager.score)

extends Node
class_name EndgameSystem

signal time_updated(value: float)

@export var _session_time_s: float
@export var _customer_count_threshold: int

var _timer: Timer

func _ready() -> void:
    self._timer = Timer.new()
    self._timer.one_shot = true
    self._timer.timeout.connect(self._return_to_main)
    self.add_child(self._timer)

func _process(_delta: float) -> void:
    var customers = self.get_tree().get_nodes_in_group(EntityTag.customer_entity)
    if customers.size() < self._customer_count_threshold:
        return
    
    if self._timer.is_stopped():
        self._timer.start(self._session_time_s)

    self.time_updated.emit(self._timer.time_left)

func _return_to_main() -> void:
    self.get_tree().change_scene_to_file("res://scenes/gui/menu.tscn")
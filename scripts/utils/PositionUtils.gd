extends RefCounted
class_name PositionUtils

static func is_in_range(node1: Node2D, node2: Node2D, min_distance: float) -> bool:
    var actual_distance = node1.position.distance_to(node2.position)
    return actual_distance <= min_distance

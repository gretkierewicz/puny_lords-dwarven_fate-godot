extends Control
class_name Tooltip

var _tooltip_heading_scene = preload("res://scenes/gui/tooltip_heading.tscn")
var _tooltip_property_scene = preload("res://scenes/gui/tooltip_property.tscn")

# public methods

func clear() -> void:
    self._clear_headings()

func set_header(text: String) -> void:
    $Panel/Header.text = text

func add_heading(text: String) -> void:
    var new_heading: TooltipGroup = self._tooltip_heading_scene.instantiate()
    new_heading.group_name = text
    $Panel/Headings.add_child(new_heading)

func add_property(name_: String, value = "") -> void:
    var new_property: TooltipProperty = self._tooltip_property_scene.instantiate()
    new_property.property_name = name_
    new_property.property_value = value
    $Panel/Headings.add_child(new_property)
    
# private methods

func _clear_headings() -> void:
    for node in $"Panel/Headings".get_children():
        node.queue_free()

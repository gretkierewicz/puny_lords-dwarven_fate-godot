extends VBoxContainer
class_name TooltipGroup

var group_name: String:
    get:
        return $"Name".text
    set(value):
        $"Name".text = str(value)

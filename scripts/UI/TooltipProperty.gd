extends HBoxContainer
class_name TooltipProperty

var property_name: String:
    get:
        return $"Name".text
    set(value):
        $"Name".text = str(value)

var property_value:
    get:
        return $"Value".text
    set(value):
        $"Value".text = str(value)

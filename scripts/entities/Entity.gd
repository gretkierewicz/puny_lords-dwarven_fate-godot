extends Node2D
class_name Entity

func get_component(component_class):
    return self.find_child(component_class.type, false, false)

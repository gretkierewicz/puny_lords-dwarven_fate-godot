extends RefCounted
class_name EntityTag

const waits_on_entry = "waits_on_entry"
const follows_player = "follows player"
const takes_seat = "takes_seat"
const sits_at_table = "sits_at_table"

const customer_entity = "customer_entity"
const table_entity = "table_entity"
const interaction_entity = "interaction_entity"

static func set_tag(entity: Node, tag: String) -> void:
    var exclusive_tags = [
        EntityTag.waits_on_entry,
        EntityTag.follows_player,
        EntityTag.takes_seat,
        EntityTag.sits_at_table,
    ]

    if tag in exclusive_tags:
        exclusive_tags.erase(tag)
        for exclusive_tag in exclusive_tags:
            entity.remove_from_group(exclusive_tag)

    entity.add_to_group(tag)

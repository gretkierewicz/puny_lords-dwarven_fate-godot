extends Node
class_name EmotionalStateComponent

const type: String = "EmotionalStateComponent"

@export var _happiness: float = 0.5

var happiness: float:
    get:
        return _happiness
    set(value):
        self._happiness = clamp(value, 0.0, 1.0)

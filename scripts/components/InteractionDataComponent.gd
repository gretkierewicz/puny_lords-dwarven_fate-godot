extends Node
class_name InteractionDataComponent

const type: String = "InteractionDataComponent"

@export var initiator: Node
@export var participants: Array = []
@export var time_remaining: float

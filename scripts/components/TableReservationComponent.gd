extends Node
class_name TableReservationComponent

const type: String = "TableReservationComponent"

@export var table_id: String
@export var seat_placement: Vector2

var has_seat_assigned: bool:
    get:
        # Vector2 is default value = not assigned seat
        return seat_placement != Vector2()

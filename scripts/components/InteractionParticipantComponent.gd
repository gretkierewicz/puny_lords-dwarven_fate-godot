extends Node
class_name InteractionParticipantComponent

const type: String = "InteractionParticipantComponent"

@export var waiting_threshold: float = -1
@export var waiting_time: float = 0
@export var current_interaction: Node

# Puny Lords - Dwarven Fate

## Project Overview
Puny Lords is a side project crafted for exploration and experimentation with various architectures, patterns and algorithms. The primary objective is to foster learning opportunities in coding, algorithm implementation, identifying bottlenecks, and developing strategies to overcome them.

## Game Idea
Initially, *Dwarven Fate* was an open-ended prototype inspired by *Dwarf Fortress* and *Zeus: Master of Olympus*, blending colony management with city-building. However, the concept has since shifted toward a more focused prototype centered on tavern management.

Players now take on the role of a tavern keeper, responsible for seating customers while considering their personality traits and preferences. The challenge lies in arranging guests in a way that ensures harmony, maximizing satisfaction, and maintaining an efficient flow of service.

## Deploy
The game is publicly available at [itch.io](https://gretkierewicz.itch.io/puny-lords-dwarven-fate). Currently, deployments are performed manually and on an irregular basis.

## Dependencies
- [Godot v4.3-stable](https://godotengine.org/download/archive/4.3-stable/) (Windows Standard version)
- Godot addons (as godot lacks package manager, those are included in current VCS):
  - [Better Terrain 1.1](https://godotengine.org/asset-library/asset/1806) addon v- (Terrain plugin for Godot 4's tilemap)
  - [gdUnit4 4.4.1](https://github.com/MikeSchulze/gdUnit4) addon v4.2.5 (Godot Embedded Unit Test Framework)

---

By maintaining an iterative learning approach and striving for continual improvement, Puny Lords aims to evolve through progressive refinements and a quest for knowledge.
